#!/bin/bash
echo "Changinf TZ into ${TZ}" >> "/tmp/changedtimezone.log"
rm -f /etc/localtime
cp -f "/usr/share/zoneinfo/${TZ}" /etc/localtime
exec "/usr/sbin/init"

